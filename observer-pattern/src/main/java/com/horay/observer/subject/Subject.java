package com.horay.observer.subject;

import com.horay.observer.observer.Observer;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 20:13
 */
public interface Subject {

	void registObserver(Observer o);

	void removeObserver(Observer o);

	void noticeObservers(Object object);

}
