package com.horay.jkxy.decorator.decorator;

import com.horay.jkxy.decorator.coffeebar.BaseDrink;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 6:50
 */
public class Milk extends Decorator {

	public Milk(BaseDrink drink) {
		super(drink);
		this.description = "牛奶";
		this.price = 2.0F;
	}

}
