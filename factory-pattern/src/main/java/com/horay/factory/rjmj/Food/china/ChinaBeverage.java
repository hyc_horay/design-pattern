package com.horay.factory.rjmj.Food.china;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.factory.rjmj.Food.Beverage;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 7:31
 */
public class ChinaBeverage extends Beverage {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public ChinaBeverage(Integer number) {
		this.name = "中国可乐";
		this.price = 8F;
		this.number = number;
	}

}
