package com.horay.factory;

import com.horay.factory.jkxy.method.LondonOrderPizza;
import com.horay.factory.jkxy.method.NewYorkOrderPizza;
import com.horay.factory.jkxy.method.OrderPizza;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 20:16
 */
public class MethodOrderPizzaTest extends PizzaTest {

	public static void main(String[] args) throws InterruptedException {
		OrderPizza orderPizza = null;
		do {
			String pizzaType = getPizzaType();
			if (pizzaType.startsWith("London")) {
				pizzaType = pizzaType.replace("London", "");
				orderPizza = new LondonOrderPizza();
			} else if (pizzaType.startsWith("NewYork")) {
				pizzaType = pizzaType.replace("NewYork", "");
				orderPizza = new NewYorkOrderPizza();
			} else {
				continue;
			}
			if ("exit".equals(pizzaType)) {
				break;
			} else {
				orderPizza.getPizza(pizzaType);
			}
		} while (true);
	}

}