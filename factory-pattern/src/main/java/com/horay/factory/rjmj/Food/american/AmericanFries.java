package com.horay.factory.rjmj.Food.american;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.factory.rjmj.Food.Fries;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 7:31
 */
public class AmericanFries extends Fries {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public AmericanFries(Integer number) {
		this.name = "美国薯条";
		this.price = 20F;
		this.number = number;
	}

}
