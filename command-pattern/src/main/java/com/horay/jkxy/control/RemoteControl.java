package com.horay.jkxy.control;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Stack;

import com.horay.jkxy.command.Command;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:37
 */
public class RemoteControl implements Control {

	private List<Command> onList = new ArrayList<Command>(10);
	private List<Command> offList = new ArrayList<Command>(10);
	private Stack<Command> stack = new Stack<Command>();

	@Override
	public int onSize() {
		return onList.size();
	}

	@Override
	public int offSize() {
		return offList.size();
	}

	@Override
	public void addOnCommand(Command command) {
		onList.add(command);
	}

	@Override
	public void addOffCommand(Command command) {
		offList.add(command);
	}

	@Override
	public void onButton(int solt) {
		Command command = onList.get(solt);
		if (Objects.nonNull(command)) {
			command.execute();
			stack.push(command);
		}
	}

	@Override
	public void offButton(int solt) {
		Command command = offList.get(solt);
		if (Objects.nonNull(command)) {
			command.execute();
			stack.push(command);
		}
	}

	@Override
	public void undoButton() {
		Command command = stack.pop();
		if (Objects.nonNull(command)) {
			command.undo();
		}
	}

}
