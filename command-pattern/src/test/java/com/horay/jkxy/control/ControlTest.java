package com.horay.jkxy.control;

import org.junit.Test;

import com.horay.jkxy.appliances.AirConditioning;
import com.horay.jkxy.appliances.Light;
import com.horay.jkxy.appliances.Sound;
import com.horay.jkxy.command.AirConditioningDownCommand;
import com.horay.jkxy.command.AirConditioningUpCommand;
import com.horay.jkxy.command.AppliancesOffCommand;
import com.horay.jkxy.command.AppliancesOnCommand;
import com.horay.jkxy.command.Command;
import com.horay.jkxy.command.MacroCommand;
import com.horay.jkxy.command.SoundDownCommand;
import com.horay.jkxy.command.SoundUpCommand;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:45
 */
public class ControlTest {

	@Test
	public void testControl() {
		Light ktLight = new Light("客厅");
		Light cfLight = new Light("厨房");
		Light wsjLight = new Light("卫生间");
		Sound sound = new Sound("HiFi");
		AirConditioning zyAirConditioning = new AirConditioning("中央");

		Control remoteControl = new RemoteControl();
		MacroCommand macroOnCommand = new MacroCommand();
		MacroCommand macroOffCommand = new MacroCommand();

		addOnCommand(new AppliancesOnCommand(ktLight), remoteControl, macroOnCommand);
		addOffCommand(new AppliancesOffCommand(ktLight), remoteControl, macroOffCommand);

		addOnCommand(new AppliancesOnCommand(cfLight), remoteControl, macroOnCommand);
		addOffCommand(new AppliancesOffCommand(cfLight), remoteControl, macroOffCommand);

		addOnCommand(new AppliancesOnCommand(wsjLight), remoteControl, macroOnCommand);
		addOffCommand(new AppliancesOffCommand(wsjLight), remoteControl, macroOffCommand);

		addOnCommand(new AppliancesOnCommand(sound), remoteControl, macroOnCommand);
		addOffCommand(new AppliancesOffCommand(sound), remoteControl, macroOffCommand);

		addOnCommand(new AppliancesOnCommand(zyAirConditioning), remoteControl, macroOnCommand);
		addOffCommand(new AppliancesOffCommand(zyAirConditioning), remoteControl, macroOffCommand);

		{
			remoteControl.addOnCommand(new SoundUpCommand(sound));
			remoteControl.addOffCommand(new SoundDownCommand(sound));
		}

		{
			remoteControl.addOnCommand(new AirConditioningUpCommand(zyAirConditioning));
			remoteControl.addOffCommand(new AirConditioningDownCommand(zyAirConditioning));
		}

		{
			remoteControl.addOnCommand(macroOnCommand);
			remoteControl.addOffCommand(macroOffCommand);
		}

		for (int i = 0, size = remoteControl.onSize(); i < size; i++) {
			remoteControl.onButton(i);
			remoteControl.undoButton();
		}

		for (int i = 0, size = remoteControl.offSize(); i < size; i++) {
			remoteControl.offButton(i);
			remoteControl.undoButton();
		}
	}

	private void addOnCommand(Command command, Control remoteControl, MacroCommand macroCommand) {
		remoteControl.addOnCommand(command);
		macroCommand.addCommand(command);
	}

	private void addOffCommand(Command command, Control remoteControl, MacroCommand macroCommand) {
		remoteControl.addOffCommand(command);
		macroCommand.addCommand(command);
	}

}