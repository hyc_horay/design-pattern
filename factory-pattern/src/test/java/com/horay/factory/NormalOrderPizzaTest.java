package com.horay.factory;

import com.horay.factory.jkxy.normal.OrderPizza;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 20:16
 */
public class NormalOrderPizzaTest extends PizzaTest {

	public static void main(String[] args) throws InterruptedException {
		OrderPizza orderPizza = new OrderPizza();
		do {
			String pizzaType = getPizzaType();
			if ("exit".equals(pizzaType)) {
				break;
			} else {
				orderPizza.getPizza(pizzaType);
			}
		} while (true);
	}

}