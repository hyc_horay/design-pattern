package com.horay.jkxy.command;

import com.horay.jkxy.appliances.Sound;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:27
 */
public class SoundDownCommand implements Command {

	private Sound sound;

	public SoundDownCommand(Sound sound) {
		this.sound = sound;
	}

	@Override
	public void execute() {
		sound.down();
	}

	@Override
	public void undo() {
		sound.up();
	}

}
