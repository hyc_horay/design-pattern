package com.horay.strategy.duck;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.strategy.behavior.fly.FlyBehavior;
import com.horay.strategy.behavior.quack.QuackBehavior;
import com.horay.strategy.behavior.swim.SwimBehavior;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 19:06
 */
public class BaseDuck implements Duck {

	private Logger logger = LoggerFactory.getLogger(getClass());

	protected String name;

	protected QuackBehavior quackBehavior;

	protected SwimBehavior swimBehavior;

	protected FlyBehavior flyBehavior;

	public BaseDuck(String name) {
		this.name = name;
	}

	@Override
	public void display() {
		logger.info("{}显示", name);
	}

	@Override
	public void quack() {
		if (Objects.nonNull(quackBehavior)) {
			quackBehavior.quack(this);
		} else {
			logger.info("{}不会叫");
		}
	}

	@Override
	public void swim() {
		if (Objects.nonNull(swimBehavior)) {
			swimBehavior.swim(this);
		} else {
			logger.info("{}不会游泳");
		}
	}

	@Override
	public void fly() {
		if (Objects.nonNull(flyBehavior)) {
			flyBehavior.fly(this);
		} else {
			logger.info("{}不会飞");
		}
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public QuackBehavior getQuackBehavior() {
		return quackBehavior;
	}

	public void setQuackBehavior(QuackBehavior quackBehavior) {
		this.quackBehavior = quackBehavior;
	}

	public SwimBehavior getSwimBehavior() {
		return swimBehavior;
	}

	public void setSwimBehavior(SwimBehavior swimBehavior) {
		this.swimBehavior = swimBehavior;
	}

	public FlyBehavior getFlyBehavior() {
		return flyBehavior;
	}

	public void setFlyBehavior(FlyBehavior flyBehavior) {
		this.flyBehavior = flyBehavior;
	}
}
