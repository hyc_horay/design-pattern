package com.horay.jkxy.appliances;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 10:50
 */
public class Popcorn {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public void on() {
		logger.info("开爆米花机");
	}

	public void off() {
		logger.info("关爆米花机");
	}

	public void pop() {
		logger.info("爆爆米花");
	}

}
