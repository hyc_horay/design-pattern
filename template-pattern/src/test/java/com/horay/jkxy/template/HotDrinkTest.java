package com.horay.jkxy.template;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/25 13:53
 */
public class HotDrinkTest {

	private static final Logger logger = LoggerFactory.getLogger(HotDrinkTest.class);

	public static void main(String[] args) {
		HotDrink hotDrink = new Tea();
		hotDrink.prepareRecipe();

		logger.info("==========================");

		hotDrink = new Coffee();
		hotDrink.prepareRecipe();
	}

}