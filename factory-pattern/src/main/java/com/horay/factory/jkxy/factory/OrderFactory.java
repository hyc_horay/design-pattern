package com.horay.factory.jkxy.factory;

import com.horay.factory.jkxy.pizza.Pizza;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 21:20
 */
public interface OrderFactory {

	public Pizza createPizza(String pizzaType);

}
