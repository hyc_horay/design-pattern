package com.horay.strategy.behavior.swim;

import com.horay.strategy.duck.Duck;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 19:03
 */
public interface SwimBehavior {
	void swim(Duck duck);
}
