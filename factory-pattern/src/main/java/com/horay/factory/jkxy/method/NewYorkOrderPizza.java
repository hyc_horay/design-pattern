package com.horay.factory.jkxy.method;

import com.horay.factory.jkxy.pizza.Pizza;
import com.horay.factory.jkxy.pizza.newyork.NewYorkCheesePizza;
import com.horay.factory.jkxy.pizza.newyork.NewYorkGreekPizza;
import com.horay.factory.jkxy.pizza.newyork.NewYorkPepperPizza;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 20:57
 */
public class NewYorkOrderPizza extends OrderPizza {

	@Override
	public Pizza createPizza(String pizzaType) {
		if ("cheese".equals(pizzaType)) {
			return new NewYorkCheesePizza();
		} else if ("greek".equals(pizzaType)) {
			return new NewYorkGreekPizza();
		} else if ("pepper".equals(pizzaType)) {
			return new NewYorkPepperPizza();
		}
		return null;
	}

}
