package com.horay.factory.rjmj.Food.china;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.factory.rjmj.Food.Fries;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 7:31
 */
public class ChinaFries extends Fries {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public ChinaFries(Integer number) {
		this.name = "中国薯条";
		this.price = 10F;
		this.number = number;
	}

}
