package com.horay.jkxy.command;

import com.horay.jkxy.appliances.AirConditioning;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:27
 */
public class AirConditioningDownCommand implements Command {

	private AirConditioning airConditioning;

	public AirConditioningDownCommand(AirConditioning airconditioning) {
		this.airConditioning = airconditioning;
	}

	@Override
	public void execute() {
		airConditioning.down();
	}

	@Override
	public void undo() {
		airConditioning.up();
	}

}
