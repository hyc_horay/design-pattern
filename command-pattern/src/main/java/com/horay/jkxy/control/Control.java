package com.horay.jkxy.control;

import com.horay.jkxy.command.Command;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 22:56
 */
public interface Control {

	public void addOnCommand(Command command);

	public void addOffCommand(Command command);

	public int onSize();

	public int offSize();

	public void onButton(int slot);

	public void offButton(int slot);

	public void undoButton();

}
