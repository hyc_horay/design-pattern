package com.horay.factory.rjmj.custom;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.factory.rjmj.base.AmericanKFCFactory;
import com.horay.factory.rjmj.base.ChinaKFCFactory;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/24 20:15
 */
public class CustomTest {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void testCustom() {
		Custom custom = new Custom(new ChinaKFCFactory());
		custom.getChicken(10);
		custom.getHamburger(20);
		custom.getBeverage(30);
		custom.getFries(40);

		logger.info("=======================================");

		custom = new Custom(new AmericanKFCFactory());
		custom.getChicken(10);
		custom.getHamburger(20);
		custom.getBeverage(30);
		custom.getFries(40);
	}

}