package com.horay.jkxy.template;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/25 13:49
 */
public class Coffee extends HotDrink {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	protected void brew() {
		logger.info("Brewing coffee");
	}

	@Override
	protected void addCondiments() {
		logger.info("Adding sugar and milk");
	}

}
