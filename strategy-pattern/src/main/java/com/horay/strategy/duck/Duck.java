package com.horay.strategy.duck;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 19:06
 */
public interface Duck {

	String getName();

	void display();

	void quack();

	void swim();

	void fly();

}
