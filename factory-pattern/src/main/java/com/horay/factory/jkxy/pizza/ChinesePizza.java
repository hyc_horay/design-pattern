package com.horay.factory.jkxy.pizza;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 20:13
 */
public class ChinesePizza extends Pizza {

	public ChinesePizza() {
		super("chinese");
	}

}
