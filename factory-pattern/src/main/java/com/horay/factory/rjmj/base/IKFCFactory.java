package com.horay.factory.rjmj.base;

import com.horay.factory.rjmj.Food.Beverage;
import com.horay.factory.rjmj.Food.Chicken;
import com.horay.factory.rjmj.Food.Fries;
import com.horay.factory.rjmj.Food.Hamburger;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/24 7:37
 */
public interface IKFCFactory {

	public Hamburger createHamburger(Integer number);

	public Beverage createBeverage(Integer number);

	public Fries createFries(Integer number);

	public Chicken createChicken(Integer number);

}
