package com.horay.strategy.duck;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 19:22
 */
public class RedHeadDuck extends BaseDuck {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public RedHeadDuck(String name) {
		super(name);
	}

	@Override
	public void display() {
		logger.info("{}和其他鸭子不一样", name);
	}

}
