package com.horay.jkxy.theater;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 11:11
 */
public class HomeTheaterFacadeTest {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void testHomeTheaterFacade() {
		HomeTheaterFacade homeTheaterFacade = new HomeTheaterFacade();
		homeTheaterFacade.ready();
		logger.info("==================================");
		homeTheaterFacade.play();
		logger.info("==================================");
		homeTheaterFacade.pause();
		logger.info("==================================");
		homeTheaterFacade.end();
	}

}