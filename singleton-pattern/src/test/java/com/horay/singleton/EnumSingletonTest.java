package com.horay.singleton;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 14:01
 */
public class EnumSingletonTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnumSingletonTest.class);

	public static void main(String[] args) {
		for (int i = 0; i < 100; i++) {
			new Thread(() -> {
				EnumSingleton.DatabaseFactory instance = EnumSingleton.DATABASE.getInstance();
				LOGGER.info("Thread.id={}, hashCode={}", Thread.currentThread().getId(), String.valueOf(Objects.hashCode(instance)));
			}).start();
		}
	}

}