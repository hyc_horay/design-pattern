package com.horay.jkxy.decorator.coffeebar;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.jkxy.decorator.coffee.Decaf;
import com.horay.jkxy.decorator.coffee.Espresso;
import com.horay.jkxy.decorator.coffee.LongBlack;
import com.horay.jkxy.decorator.coffee.ShortBlack;
import com.horay.jkxy.decorator.decorator.Chocolate;
import com.horay.jkxy.decorator.decorator.Milk;
import com.horay.jkxy.decorator.decorator.Soy;
import com.horay.jkxy.decorator.decorator.Sugar;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 6:54
 */
public class DrinkTest {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void testDrink() {
		BaseDrink drink = new Decaf();
		drink = new Chocolate(drink);
		drink = new Milk(drink);
		drink = new Soy(drink);
		drink = new Sugar(drink);
		logger.info("description=" + drink.getDescription());
		logger.info("price=" + drink.cost());
		logger.info("===========================");

		drink = new Espresso();
		drink = new Chocolate(drink);
		drink = new Chocolate(drink);
		drink = new Milk(drink);
		drink = new Milk(drink);
		drink = new Sugar(drink);
		drink = new Sugar(drink);
		logger.info("description=" + drink.getDescription());
		logger.info("price=" + drink.cost());
		logger.info("===========================");

		drink = new ShortBlack();
		drink = new Chocolate(drink);
		drink = new Milk(drink);
		drink = new Sugar(drink);
		logger.info("description=" + drink.getDescription());
		logger.info("price=" + drink.cost());
		logger.info("===========================");

		drink = new LongBlack();
		drink = new Chocolate(drink);
		drink = new Chocolate(drink);
		drink = new Milk(drink);
		drink = new Sugar(drink);
		logger.info("description=" + drink.getDescription());
		logger.info("price=" + drink.cost());
		logger.info("===========================");
	}

}