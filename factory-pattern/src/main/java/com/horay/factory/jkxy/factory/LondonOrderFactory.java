package com.horay.factory.jkxy.factory;

import com.horay.factory.jkxy.pizza.Pizza;
import com.horay.factory.jkxy.pizza.london.LondonCheesePizza;
import com.horay.factory.jkxy.pizza.london.LondonGreekPizza;
import com.horay.factory.jkxy.pizza.london.LondonPepperPizza;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 21:20
 */
public class LondonOrderFactory implements OrderFactory {

	@Override
	public Pizza createPizza(String pizzaType) {
		if ("cheese".equals(pizzaType)) {
			return new LondonCheesePizza();
		} else if ("greek".equals(pizzaType)) {
			return new LondonGreekPizza();
		} else if ("pepper".equals(pizzaType)) {
			return new LondonPepperPizza();
		}
		return null;
	}

}
