package com.horay.jkxy.decorator.coffee;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 6:41
 */
public class LongBlack extends Coffee {

	public LongBlack() {
		this.description = "LongBlack";
		this.price = 6.0F;
	}

}
