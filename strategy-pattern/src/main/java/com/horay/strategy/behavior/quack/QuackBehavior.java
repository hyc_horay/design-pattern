package com.horay.strategy.behavior.quack;

import com.horay.strategy.duck.Duck;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 19:05
 */
public interface QuackBehavior {
	void quack(Duck duck);
}
