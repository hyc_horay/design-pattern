package com.horay.factory.rjmj.custom;

import com.horay.factory.rjmj.Food.Beverage;
import com.horay.factory.rjmj.Food.Chicken;
import com.horay.factory.rjmj.Food.Fries;
import com.horay.factory.rjmj.Food.Hamburger;
import com.horay.factory.rjmj.base.IKFCFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 20:11
 */
public class Custom {

	private IKFCFactory ikfcFactory;

	public Custom(IKFCFactory ikfcFactory) {
		this.ikfcFactory = ikfcFactory;
	}

	public void getHamburger(Integer number) {
		Hamburger hamburger = ikfcFactory.createHamburger(number);
		hamburger.printMessage();
	}

	public void getBeverage(Integer number) {
		Beverage beverage = ikfcFactory.createBeverage(number);
		beverage.printMessage();
	}

	public void getChicken(Integer number) {
		Chicken chicken = ikfcFactory.createChicken(number);
		chicken.printMessage();
	}

	public void getFries(Integer number) {
		Fries fries = ikfcFactory.createFries(number);
		fries.printMessage();
	}

	public IKFCFactory getIkfcFactory() {
		return ikfcFactory;
	}

	public void setIkfcFactory(IKFCFactory ikfcFactory) {
		this.ikfcFactory = ikfcFactory;
	}
}
