package com.horay.jkxy.command;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:14
 */
public interface Command {

	public void execute();

	public void undo();

}
