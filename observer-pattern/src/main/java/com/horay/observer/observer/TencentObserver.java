package com.horay.observer.observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.observer.model.WeatherData;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 20:23
 */
public class TencentObserver implements Observer {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void update(Object object) {
		WeatherData weatherData = (WeatherData) object;
		logger.info("{}：今天的温度{}℃", TencentObserver.class.getName(), weatherData.getTemperature());
		logger.info("{}：今天的湿度{}%", TencentObserver.class.getName(), weatherData.getHumidity());
		logger.info("{}：今天的气压{}%", TencentObserver.class.getName(), weatherData.getPressure());
	}
}
