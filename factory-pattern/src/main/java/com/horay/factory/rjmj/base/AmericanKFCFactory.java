package com.horay.factory.rjmj.base;

import com.horay.factory.rjmj.Food.Beverage;
import com.horay.factory.rjmj.Food.Chicken;
import com.horay.factory.rjmj.Food.Fries;
import com.horay.factory.rjmj.Food.Hamburger;
import com.horay.factory.rjmj.Food.american.AmericanBeverage;
import com.horay.factory.rjmj.Food.american.AmericanChicken;
import com.horay.factory.rjmj.Food.american.AmericanFries;
import com.horay.factory.rjmj.Food.american.AmericanHamburger;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/24 20:01
 */
public class AmericanKFCFactory implements IKFCFactory {

	@Override
	public Hamburger createHamburger(Integer number) {
		return new AmericanHamburger(number);
	}

	@Override
	public Beverage createBeverage(Integer number) {
		return new AmericanBeverage(number);
	}

	@Override
	public Fries createFries(Integer number) {
		return new AmericanFries(number);
	}

	@Override
	public Chicken createChicken(Integer number) {
		return new AmericanChicken(number);
	}

}
