package com.horay.jkxy.command;

import com.horay.jkxy.appliances.AirConditioning;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:27
 */
public class AirConditioningUpCommand implements Command {

	private AirConditioning airconditioning;

	public AirConditioningUpCommand(AirConditioning airconditioning) {
		this.airconditioning = airconditioning;
	}

	@Override
	public void execute() {
		airconditioning.up();
	}

	@Override
	public void undo() {
		airconditioning.down();
	}

}
