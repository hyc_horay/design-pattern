package com.horay.jkxy.command;

import com.horay.jkxy.appliances.Appliances;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:27
 */
public class AppliancesOffCommand implements Command {

	private Appliances appliances;

	public AppliancesOffCommand(Appliances appliances) {
		this.appliances = appliances;
	}

	@Override
	public void execute() {
		appliances.off();
	}

	@Override
	public void undo() {
		appliances.on();
	}

}
