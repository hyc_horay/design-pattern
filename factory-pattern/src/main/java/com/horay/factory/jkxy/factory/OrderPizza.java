package com.horay.factory.jkxy.factory;

import java.util.Objects;

import com.horay.factory.jkxy.pizza.Pizza;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 20:07
 */
public class OrderPizza {

	private OrderFactory orderFactory;

	public void getPizza(String pizzaType) throws InterruptedException {
		Pizza pizza = orderFactory.createPizza(pizzaType);
		if (Objects.nonNull(pizza)) {
			pizza.prepare();
			pizza.bake();
			pizza.cut();
			pizza.box();
		}
	}

	public OrderFactory getOrderFactory() {
		return orderFactory;
	}

	public void setOrderFactory(OrderFactory orderFactory) {
		this.orderFactory = orderFactory;
	}

}
