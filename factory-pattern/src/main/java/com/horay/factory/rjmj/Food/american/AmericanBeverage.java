package com.horay.factory.rjmj.Food.american;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.factory.rjmj.Food.Beverage;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 7:31
 */
public class AmericanBeverage extends Beverage {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public AmericanBeverage(Integer number) {
		this.name = "美国可乐";
		this.price = 18F;
		this.number = number;
	}

}
