package com.horay.singleton;

import java.util.concurrent.TimeUnit;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/23 13:55
 */
public enum EnumSingleton {

	DATABASE;

	private DatabaseFactory databaseFactory;

	private EnumSingleton() {
		databaseFactory = new DatabaseFactory();
	}

	public DatabaseFactory getInstance() {
		return databaseFactory;
	}

	public class DatabaseFactory {
		private DatabaseFactory() {
			try {
				TimeUnit.SECONDS.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
