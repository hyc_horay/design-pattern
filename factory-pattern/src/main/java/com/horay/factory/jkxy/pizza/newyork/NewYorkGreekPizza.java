package com.horay.factory.jkxy.pizza.newyork;

import com.horay.factory.jkxy.pizza.Pizza;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 20:13
 */
public class NewYorkGreekPizza extends Pizza {

	public NewYorkGreekPizza() {
		super("NewYork greek");
	}

}
