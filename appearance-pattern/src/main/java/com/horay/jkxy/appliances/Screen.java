package com.horay.jkxy.appliances;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 10:50
 */
public class Screen {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public void up() {
		logger.info("展开幕布");
	}

	public void down() {
		logger.info("收起幕布");
	}

}
