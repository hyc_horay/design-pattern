package com.horay.jkxy.template;

import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/25 13:49
 */
public class Tea extends HotDrink {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	protected void brew() {
		logger.info("Brewing tea");
	}

	@Override
	protected boolean wantaddCondiments() {
		Scanner scanner = new Scanner(System.in);
		logger.info("Condiments yes or no, please input y/n:");
		if (scanner.hasNextLine()) {
			String s = scanner.nextLine();
			if ("y".equals(s)) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	@Override
	protected void addCondiments() {
		logger.info("Adding lemon");
	}

}
