package com.horay.jkxy.template;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/25 13:44
 */
public abstract class HotDrink {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public final void prepareRecipe() {
		this.boilWater();
		this.brew();
		this.pourInCup();
		if (wantaddCondiments()) {
			this.addCondiments();
		} else {
			logger.info("No condiments");
		}
	}

	protected boolean wantaddCondiments() {
		return true;
	}

	protected final void boilWater() {
		logger.info("Boiling Water...");
	}

	protected final void pourInCup() {
		logger.info("Pouring in cup...");
	}

	protected abstract void brew();

	protected abstract void addCondiments();

}
