package com.horay.jkxy.adapter;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 0:20
 */
public class EnumerationAdapter<E> implements Iterator {

	private Enumeration enumeration;

	public EnumerationAdapter(Enumeration enumeration) {
		this.enumeration = enumeration;
	}

	@Override
	public boolean hasNext() {
		return enumeration.hasMoreElements();
	}

	@Override
	public E next() {
		return (E) enumeration.nextElement();
	}

	@Override
	public void remove() {
		new UnsupportedOperationException();
	}

}
