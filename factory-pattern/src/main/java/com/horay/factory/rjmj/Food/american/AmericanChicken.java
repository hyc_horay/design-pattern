package com.horay.factory.rjmj.Food.american;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.factory.rjmj.Food.Chicken;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 7:31
 */
public class AmericanChicken extends Chicken {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public AmericanChicken(Integer number) {
		this.name = "美国炸鸡";
		this.price = 19F;
		this.number = number;
	}

}
