package com.horay.jkxy.theater;

import com.horay.jkxy.appliances.DVDPlayer;
import com.horay.jkxy.appliances.Popcorn;
import com.horay.jkxy.appliances.Projector;
import com.horay.jkxy.appliances.Screen;
import com.horay.jkxy.appliances.Stereo;
import com.horay.jkxy.appliances.TheaterLights;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 11:02
 */
public class HomeTheaterFacade {

	private Popcorn popcorn = new Popcorn();

	private Projector projector = new Projector();

	private Screen screen = new Screen();

	private Stereo stereo = new Stereo();

	private TheaterLights theaterLights = new TheaterLights();

	private DVDPlayer dvdPlayer = new DVDPlayer();

	public void ready() {
		popcorn.on();
		projector.on();
		screen.up();
		stereo.on();
		theaterLights.on();
		theaterLights.dim();
		dvdPlayer.on();
		dvdPlayer.setDvd();
	}

	public void end() {
		popcorn.off();
		projector.off();
		screen.down();
		stereo.off();
		theaterLights.off();
		dvdPlayer.off();
	}

	public void play() {
		dvdPlayer.play();
	}

	public void pause() {
		dvdPlayer.pause();
	}

}
