package com.horay.jkxy.adapter;

import org.junit.Test;

import com.horay.jkxy.animal.Duck;
import com.horay.jkxy.animal.WildTurkey;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 0:17
 */
public class ObjectAdapterTest {

	@Test
	public void testObjectAdapter() {
		Duck duck = new ObjectAdapter(new WildTurkey());
		duck.quack();
		duck.fly();
	}

}