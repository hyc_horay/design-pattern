package com.horay.factory.jkxy.simple;

import com.horay.factory.jkxy.pizza.CheesePizza;
import com.horay.factory.jkxy.pizza.ChinesePizza;
import com.horay.factory.jkxy.pizza.GreekPizza;
import com.horay.factory.jkxy.pizza.PepperPizza;
import com.horay.factory.jkxy.pizza.Pizza;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/23 20:36
 */
public class SimpleFactory {
	public Pizza createPizza(String pizzaType) {
		if ("cheese".equals(pizzaType)) {
			return new CheesePizza();
		} else if ("greek".equals(pizzaType)) {
			return new GreekPizza();
		} else if ("pepper".equals(pizzaType)) {
			return new PepperPizza();
		} else if ("chinese".equals(pizzaType)) {
			return new ChinesePizza();
		}
		return null;
	}
}
