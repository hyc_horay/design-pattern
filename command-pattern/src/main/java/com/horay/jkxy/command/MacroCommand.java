package com.horay.jkxy.command;

import java.util.ArrayList;
import java.util.List;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:27
 */
public class MacroCommand implements Command {

	private List<Command> list = new ArrayList<Command>(10);

	public void addCommand(Command command) {
		list.add(command);
	}

	@Override
	public void execute() {
		for (Command command : list) {
			command.execute();
		}
	}

	@Override
	public void undo() {
		for (Command command : list) {
			command.undo();
		}
	}

}
