package com.horay.observer.subject;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.observer.observer.BaiduObserver;
import com.horay.observer.observer.SinaObserver;
import com.horay.observer.observer.TencentObserver;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 20:45
 */
public class ObserverTest {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void testObserver() {

		WeatherDataSubject subject = new WeatherDataSubject();
		BaiduObserver baiduObserver = new BaiduObserver();
		TencentObserver tencentObserver = new TencentObserver();
		SinaObserver sinaObserver = new SinaObserver();

		subject.registObserver(baiduObserver);
		subject.registObserver(tencentObserver);
		subject.registObserver(sinaObserver);
		subject.setValue(15, 57, 1022);
		logger.info("=================================");

		subject.removeObserver(baiduObserver);
		subject.setValue(25, 67, 1122);
		logger.info("=================================");

		subject.removeObserver(tencentObserver);
		subject.setValue(35, 77, 1222);
		logger.info("=================================");

		subject.removeObserver(sinaObserver);
		subject.setValue(45, 87, 1322);
		logger.info("=================================");

	}

}