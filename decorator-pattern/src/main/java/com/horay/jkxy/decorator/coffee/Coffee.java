package com.horay.jkxy.decorator.coffee;

import com.horay.jkxy.decorator.coffeebar.BaseDrink;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 6:39
 */
public class Coffee extends BaseDrink {

	@Override
	public Float cost() {
		return this.price;
	}

}
