package com.horay.jkxy.appliances;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:19
 */
public class Sound extends Appliances {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private Integer volume = 0;

	public Sound(String name) {
		super(name);
	}

	public void up() {
		volume++;
		logger.info("{}音量={}", name, volume);
	}

	public void down() {
		volume--;
		logger.info("{}音量={}", name, volume);
	}

}
