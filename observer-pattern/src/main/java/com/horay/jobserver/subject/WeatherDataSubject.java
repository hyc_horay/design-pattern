package com.horay.jobserver.subject;

import java.util.Observable;

import com.horay.observer.model.WeatherData;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 20:24
 */
public class WeatherDataSubject extends Observable {

	private Integer temperature;

	private Integer humidity;

	private Integer pressure;

	public void setValue(Integer temperature, Integer humidity, Integer pressure) {
		this.temperature = temperature;
		this.humidity = humidity;
		this.pressure = pressure;
		this.setChanged();
		this.notifyObservers(new WeatherData(temperature, humidity, pressure));
	}

	public Integer getTemperature() {
		return temperature;
	}

	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}

	public Integer getHumidity() {
		return humidity;
	}

	public void setHumidity(Integer humidity) {
		this.humidity = humidity;
	}

	public Integer getPressure() {
		return pressure;
	}

	public void setPressure(Integer pressure) {
		this.pressure = pressure;
	}
}
