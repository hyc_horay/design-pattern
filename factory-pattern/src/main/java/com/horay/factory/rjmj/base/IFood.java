package com.horay.factory.rjmj.base;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/24 7:26
 */
public interface IFood {

	public void printMessage();

}
