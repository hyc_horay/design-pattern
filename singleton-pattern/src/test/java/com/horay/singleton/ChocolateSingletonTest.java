package com.horay.singleton;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 14:01
 */
public class ChocolateSingletonTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChocolateSingletonTest.class);

	public static void main(String[] args) {
		for (int i = 0; i < 100; i++) {
			new Thread(() -> {
				ChocolateSingleton instance = ChocolateSingleton.getInstance();
				LOGGER.info("Thread.id={}, hashCode={}", Thread.currentThread().getId(), String.valueOf(Objects.hashCode(instance)));
			}).start();
		}
	}

}