package com.horay.jobserver.subject;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.jobserver.observer.BaiduObserver;
import com.horay.jobserver.observer.SinaObserver;
import com.horay.jobserver.observer.TencentObserver;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 21:20
 */
public class SubjectTest {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void testObserver() {

		WeatherDataSubject subject = new WeatherDataSubject();
		BaiduObserver baiduObserver = new BaiduObserver();
		TencentObserver tencentObserver = new TencentObserver();
		SinaObserver sinaObserver = new SinaObserver();

		subject.addObserver(baiduObserver);
		subject.addObserver(tencentObserver);
		subject.addObserver(sinaObserver);
		subject.setValue(15, 57, 1022);
		logger.info("=================================");

		subject.deleteObserver(baiduObserver);
		subject.setValue(25, 67, 1122);
		logger.info("=================================");

		subject.deleteObserver(tencentObserver);
		subject.setValue(35, 77, 1222);
		logger.info("=================================");

		subject.deleteObserver(sinaObserver);
		subject.setValue(45, 87, 1322);
		logger.info("=================================");

	}

}