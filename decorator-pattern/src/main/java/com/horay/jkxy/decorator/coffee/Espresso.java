package com.horay.jkxy.decorator.coffee;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 6:41
 */
public class Espresso extends Coffee {

	public Espresso() {
		this.description = "Espresso";
		this.price = 4.0F;
	}

}
