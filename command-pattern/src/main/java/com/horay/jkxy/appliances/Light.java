package com.horay.jkxy.appliances;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:16
 */
public class Light extends Appliances {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public Light(String name) {
		super(name);
	}
}
