package com.horay.singleton;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/23 11:09
 */
public class InnerClassSingleton {

	private InnerClassSingleton() {
	}

	private static class SingletonHolder {
		private static InnerClassSingleton innerClassSingleton = new InnerClassSingleton();

		private SingletonHolder() {
		}
	}

	public static InnerClassSingleton getInstance() {
		return SingletonHolder.innerClassSingleton;
	}

}
