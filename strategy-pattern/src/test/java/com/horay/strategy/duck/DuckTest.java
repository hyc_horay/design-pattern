package com.horay.strategy.duck;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.strategy.behavior.fly.FlyBehavior;
import com.horay.strategy.behavior.fly.NonFlyBehavior;
import com.horay.strategy.behavior.fly.NorthFlyBehavior;
import com.horay.strategy.behavior.fly.SouthFlyBehavior;
import com.horay.strategy.behavior.quack.BiBiQuackBehavior;
import com.horay.strategy.behavior.quack.GuGuQuackBehavior;
import com.horay.strategy.behavior.quack.GuaGuaQuackBehavior;
import com.horay.strategy.behavior.quack.QuackBehavior;
import com.horay.strategy.behavior.swim.ControlSwimBehavior;
import com.horay.strategy.behavior.swim.HeadInSwimBehavior;
import com.horay.strategy.behavior.swim.HeadOutSwimBehavior;
import com.horay.strategy.behavior.swim.SwimBehavior;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/22 19:48
 */
public class DuckTest {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Test
	public void testDuck() {

		FlyBehavior nonFlyBehavior = new NonFlyBehavior();
		FlyBehavior northFlyBehavior = new NorthFlyBehavior();
		FlyBehavior southFlyBehavior = new SouthFlyBehavior();

		QuackBehavior guaGuaQuackBehavior = new GuaGuaQuackBehavior();
		QuackBehavior guGuQuackBehavior = new GuGuQuackBehavior();
		QuackBehavior biBiQuackBehavior = new BiBiQuackBehavior();

		SwimBehavior headInSwimBehavior = new HeadInSwimBehavior();
		SwimBehavior headOutSwimBehavior = new HeadOutSwimBehavior();
		SwimBehavior controlSwimBehavior = new ControlSwimBehavior();

		BaseDuck toyDuck = new ToyDuck("玩具鸭");
		toyDuck.setFlyBehavior(nonFlyBehavior);
		toyDuck.setSwimBehavior(controlSwimBehavior);
		toyDuck.setQuackBehavior(biBiQuackBehavior);
		toyDuck.fly();
		toyDuck.swim();
		toyDuck.quack();

		logger.info("==========================================");

		BaseDuck redHeadDuck = new RedHeadDuck("红头鸭");
		redHeadDuck.setFlyBehavior(northFlyBehavior);
		redHeadDuck.setSwimBehavior(headOutSwimBehavior);
		redHeadDuck.setQuackBehavior(guGuQuackBehavior);
		redHeadDuck.fly();
		redHeadDuck.swim();
		redHeadDuck.quack();

		logger.info("==========================================");

		BaseDuck greenHeadDuck = new GreenHeadDuck("绿头鸭");
		greenHeadDuck.setFlyBehavior(southFlyBehavior);
		greenHeadDuck.setSwimBehavior(headInSwimBehavior);
		greenHeadDuck.setQuackBehavior(guaGuaQuackBehavior);
		greenHeadDuck.fly();
		greenHeadDuck.swim();
		greenHeadDuck.quack();

	}

}