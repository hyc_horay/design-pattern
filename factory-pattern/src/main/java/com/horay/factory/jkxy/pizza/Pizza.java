package com.horay.factory.jkxy.pizza;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 20:04
 */
public class Pizza {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private String name;

	public Pizza(String name) {
		this.name = name;
	}

	public void prepare() {
		logger.info("{} prepare", name);
	}

	public void bake() {
		logger.info("{} bake", name);
	}

	public void cut() {
		logger.info("{} cut", name);
	}

	public void box() {
		logger.info("{} box", name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
