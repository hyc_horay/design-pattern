package com.horay.factory;

import com.horay.factory.jkxy.simple.OrderPizza;
import com.horay.factory.jkxy.simple.SimpleFactory;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 20:16
 */
public class SimpleFactoryOrderPizzaTest extends PizzaTest {

	public static void main(String[] args) throws InterruptedException {
		OrderPizza orderPizza = new OrderPizza();
		SimpleFactory simpleFactory = new SimpleFactory();
		orderPizza.setSimpleFactory(simpleFactory);
		do {
			String pizzaType = getPizzaType();
			if ("exit".equals(pizzaType)) {
				break;
			} else {
				orderPizza.getPizza(pizzaType);
			}
		} while (true);
	}

}