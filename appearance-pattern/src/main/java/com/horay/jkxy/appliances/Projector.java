package com.horay.jkxy.appliances;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 10:50
 */
public class Projector {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public void on() {
		logger.info("开投影机");
	}

	public void off() {
		logger.info("关投影机");
	}

	public void focus() {
		logger.info("调焦距");
	}

	public void zoom() {
		logger.info("调缩放");
	}

}
