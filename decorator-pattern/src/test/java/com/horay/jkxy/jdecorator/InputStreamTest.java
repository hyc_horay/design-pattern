package com.horay.jkxy.jdecorator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/23 7:36
 */
public class InputStreamTest {

	@Test
	public void testInputStream() throws FileNotFoundException {
		int c;
		try {
			InputStream in = new UpperCaseInputStream(new FileInputStream("D:/test.txt"));
			while ((c = in.read()) >= 0) {
				System.out.print((char) c);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}