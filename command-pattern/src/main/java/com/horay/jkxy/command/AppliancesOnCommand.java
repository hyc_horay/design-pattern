package com.horay.jkxy.command;

import com.horay.jkxy.appliances.Appliances;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:27
 */
public class AppliancesOnCommand implements Command {

	private Appliances appliances;

	public AppliancesOnCommand(Appliances appliances) {
		this.appliances = appliances;
	}

	@Override
	public void execute() {
		appliances.on();
	}

	@Override
	public void undo() {
		appliances.off();
	}

}
