package com.horay.observer.observer;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 20:15
 */
public interface Observer {

	void update(Object object);

}
