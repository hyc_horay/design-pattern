package com.horay.strategy.behavior.fly;

import com.horay.strategy.duck.Duck;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 19:09
 */
public interface FlyBehavior {
	void fly(Duck duck);
}
