package com.horay.strategy.behavior.quack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.strategy.duck.Duck;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 19:38
 */
public class GuGuQuackBehavior implements QuackBehavior {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void quack(Duck duck) {
		logger.info("{}咕咕地叫", duck.getName());
	}
}
