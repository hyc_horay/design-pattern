package com.horay.jkxy.jdecorator;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/23 7:30
 */
public class UpperCaseInputStream extends FilterInputStream {

	protected UpperCaseInputStream(InputStream in) {
		super(in);
	}

	@Override
	public int read() throws IOException {
		int c = super.read();
		return c == -1 ? c : Character.toUpperCase((char) (c));
	}

	@Override
	public int read(byte[] b, int offset, int len) throws IOException {
		int result = super.read(b, offset, len);
		for (int i = 0; i < result; i++) {
			b[i] = (byte) Character.toUpperCase((char) (b[i]));
		}
		return result;
	}

}
