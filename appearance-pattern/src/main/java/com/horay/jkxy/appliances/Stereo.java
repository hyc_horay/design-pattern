package com.horay.jkxy.appliances;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 10:50
 */
public class Stereo {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private Integer volume = 0;

	public void on() {
		logger.info("开音响");
	}

	public void off() {
		logger.info("关音响");
	}

	public void up() {
		volume++;
		logger.info("调大音响");
	}

	public void down() {
		volume--;
		logger.info("调小音响");
	}

	public Integer getVolume() {
		return volume;
	}

}
