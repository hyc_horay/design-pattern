package com.horay.jkxy.command;

import com.horay.jkxy.appliances.AirConditioning;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:27
 */
public class AirConditioningOnCommand implements Command {

	private AirConditioning airConditioning;

	public AirConditioningOnCommand(AirConditioning airConditioning) {
		this.airConditioning = airConditioning;
	}

	@Override
	public void execute() {
		airConditioning.on();
	}

	@Override
	public void undo() {
		airConditioning.off();
	}

}
