package com.horay.singleton;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/23 14:29
 */
public class InnerClassSingletonTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(InnerClassSingletonTest.class);

	public static void main(String[] args) {
		for (int i = 0; i < 100; i++) {
			new Thread(() -> {
				InnerClassSingleton instance = InnerClassSingleton.getInstance();
				LOGGER.info("Thread.id={}, hashCode={}", Thread.currentThread().getId(), String.valueOf(Objects.hashCode(instance)));
			}).start();
		}
	}

}