package com.horay.jkxy.decorator.decorator;

import com.horay.jkxy.decorator.coffeebar.BaseDrink;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 6:47
 */
public class Decorator extends BaseDrink {

	protected BaseDrink drink;

	public Decorator() {
	}

	public Decorator(BaseDrink drink) {
		this.drink = drink;
	}

	@Override
	public Float cost() {
		return this.price + drink.cost();
	}

	@Override
	public String getDescription() {
		return this.description + "-" + this.price + "&&" + drink.getDescription();
	}

	public BaseDrink getDrink() {
		return drink;
	}

	public void setDrink(BaseDrink drink) {
		this.drink = drink;
	}
}
