package com.horay.jkxy.decorator.coffee;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 6:41
 */
public class Decaf extends Coffee {

	public Decaf() {
		this.description = "Decaf";
		this.price = 3F;
	}

}
