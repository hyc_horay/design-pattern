package com.horay.jkxy.animal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 0:12
 */
public class WildTurkey implements Turkey {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void called() {
		logger.info("gu gu");
	}

	@Override
	public void fly() {
		logger.info("short distance");
	}

}
