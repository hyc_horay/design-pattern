package com.horay.factory.rjmj.Food;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.factory.rjmj.base.BaseFood;
import com.horay.factory.rjmj.base.IFood;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 7:31
 */
public class Hamburger extends BaseFood implements IFood {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void printMessage() {
		logger.info("{}汉堡包, 单价：{}, 数量：{}, 总价：{}", name, price, number, total());
	}
}
