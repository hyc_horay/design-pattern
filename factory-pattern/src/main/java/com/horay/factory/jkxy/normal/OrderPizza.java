package com.horay.factory.jkxy.normal;

import com.horay.factory.jkxy.pizza.CheesePizza;
import com.horay.factory.jkxy.pizza.ChinesePizza;
import com.horay.factory.jkxy.pizza.GreekPizza;
import com.horay.factory.jkxy.pizza.PepperPizza;
import com.horay.factory.jkxy.pizza.Pizza;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/23 20:07
 */
public class OrderPizza {

	public void getPizza(String pizzaType) throws InterruptedException {
		Pizza pizza = null;
		if ("cheese".equals(pizzaType)) {
			pizza = new CheesePizza();
		} else if ("greek".equals(pizzaType)) {
			pizza = new GreekPizza();
		} else if ("pepper".equals(pizzaType)) {
			pizza = new PepperPizza();
		} else if ("chinese".equals(pizzaType)) {
			pizza = new ChinesePizza();
		}
		pizza.prepare();
		pizza.bake();
		pizza.cut();
		pizza.box();
	}

}
