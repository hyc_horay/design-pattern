package com.horay.jkxy.decorator.decorator;

import com.horay.jkxy.decorator.coffeebar.BaseDrink;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 6:50
 */
public class Soy extends Decorator {

	public Soy(BaseDrink drink) {
		super(drink);
		this.description = "豆浆";
		this.price = 2.0F;
	}

}
