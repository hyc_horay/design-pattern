package com.horay.jkxy.command;

import com.horay.jkxy.appliances.Sound;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:27
 */
public class SoundOnCommand implements Command {

	private Sound sound;

	public SoundOnCommand(Sound sound) {
		this.sound = sound;
	}

	@Override
	public void execute() {
		sound.on();
	}

	@Override
	public void undo() {
		sound.off();
	}

}
