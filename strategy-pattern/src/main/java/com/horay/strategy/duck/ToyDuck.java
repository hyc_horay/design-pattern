package com.horay.strategy.duck;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 19:22
 */
public class ToyDuck extends BaseDuck {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public ToyDuck(String name) {
		super(name);
	}

}
