package com.horay.factory.jkxy.method;

import java.util.Objects;

import com.horay.factory.jkxy.pizza.Pizza;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 20:07
 */
public abstract class OrderPizza {

	public void getPizza(String pizzaType) throws InterruptedException {
		Pizza pizza = createPizza(pizzaType);
		if (Objects.nonNull(pizza)) {
			pizza.prepare();
			pizza.bake();
			pizza.cut();
			pizza.box();
		}
	}

	public abstract Pizza createPizza(String pizzaType);

}
