package com.horay.strategy.behavior.fly;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.strategy.duck.Duck;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 19:28
 */
public class SouthFlyBehavior implements FlyBehavior {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void fly(Duck duck) {
		logger.info("{}一直向南飞", duck.getName());
	}

}
