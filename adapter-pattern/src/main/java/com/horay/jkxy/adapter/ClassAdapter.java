package com.horay.jkxy.adapter;

import com.horay.jkxy.animal.Duck;
import com.horay.jkxy.animal.WildTurkey;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 0:14
 */
public class ClassAdapter extends WildTurkey implements Duck {

	@Override
	public void quack() {
		super.called();
	}

	@Override
	public void fly() {
		super.fly();
		super.fly();
		super.fly();
	}

}
