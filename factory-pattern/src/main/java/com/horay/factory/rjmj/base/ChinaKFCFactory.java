package com.horay.factory.rjmj.base;

import com.horay.factory.rjmj.Food.Beverage;
import com.horay.factory.rjmj.Food.Chicken;
import com.horay.factory.rjmj.Food.Fries;
import com.horay.factory.rjmj.Food.Hamburger;
import com.horay.factory.rjmj.Food.china.ChinaBeverage;
import com.horay.factory.rjmj.Food.china.ChinaChicken;
import com.horay.factory.rjmj.Food.china.ChinaFries;
import com.horay.factory.rjmj.Food.china.ChinaHamburger;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/24 20:01
 */
public class ChinaKFCFactory implements IKFCFactory {

	@Override
	public Hamburger createHamburger(Integer number) {
		return new ChinaHamburger(number);
	}

	@Override
	public Beverage createBeverage(Integer number) {
		return new ChinaBeverage(number);
	}

	@Override
	public Fries createFries(Integer number) {
		return new ChinaFries(number);
	}

	@Override
	public Chicken createChicken(Integer number) {
		return new ChinaChicken(number);
	}

}
