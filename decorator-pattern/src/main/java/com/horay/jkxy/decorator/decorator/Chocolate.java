package com.horay.jkxy.decorator.decorator;

import com.horay.jkxy.decorator.coffeebar.BaseDrink;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 6:50
 */
public class Chocolate extends Decorator {

	public Chocolate(BaseDrink drink) {
		super(drink);
		this.description = "巧克力";
		this.price = 3.0F;
	}

}
