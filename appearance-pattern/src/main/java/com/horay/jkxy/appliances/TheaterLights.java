package com.horay.jkxy.appliances;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 10:50
 */
public class TheaterLights {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public void on() {
		logger.info("开灯");
	}

	public void off() {
		logger.info("关灯");
	}

	public void dim() {
		logger.info("调暗光");
	}

	public void bright() {
		logger.info("调亮光");
	}

}
