package com.horay.factory.jkxy.simple;

import java.util.Objects;

import com.horay.factory.jkxy.pizza.Pizza;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 20:07
 */
public class OrderPizza {

	private SimpleFactory simpleFactory;

	public void getPizza(String pizzaType) throws InterruptedException {
		Pizza pizza = simpleFactory.createPizza(pizzaType);
		if (Objects.nonNull(pizza)) {
			pizza.prepare();
			pizza.bake();
			pizza.cut();
			pizza.box();
		}
	}

	public SimpleFactory getSimpleFactory() {
		return simpleFactory;
	}

	public void setSimpleFactory(SimpleFactory simpleFactory) {
		this.simpleFactory = simpleFactory;
	}
}
