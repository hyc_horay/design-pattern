package com.horay.jkxy.decorator.coffee;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 6:41
 */
public class ShortBlack extends Coffee {

	public ShortBlack() {
		this.description = "ShortBlack";
		this.price = 6.0F;
	}

}
