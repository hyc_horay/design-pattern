package com.horay.factory.rjmj.Food.american;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.factory.rjmj.Food.Hamburger;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 7:31
 */
public class AmericanHamburger extends Hamburger {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public AmericanHamburger(Integer number) {
		this.name = "美国汉堡包";
		this.price = 25.5F;
		this.number = number;
	}

}
