package com.horay.observer.subject;

import java.util.ArrayList;
import java.util.List;

import com.horay.observer.observer.Observer;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 20:18
 */
public abstract class BaseSubject implements Subject {

	private List<Observer> observers = new ArrayList<Observer>();

	@Override
	public void registObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void removeObserver(Observer o) {
		observers.remove(o);
	}

	@Override
	public void noticeObservers(Object object) {
		for (Observer o : observers) {
			o.update(object);
		}
	}
}
