package com.horay.jkxy.animal;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 0:12
 */
public interface Turkey {

	public void called();

	public void fly();

}
