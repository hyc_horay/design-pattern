package com.horay.jkxy.adapter;

import org.junit.Test;

import com.horay.jkxy.animal.Duck;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 0:16
 */
public class ClassAdapterTest {

	@Test
	public void testClassAdapter() {
		Duck duck = new ClassAdapter();
		duck.quack();
		duck.fly();
	}

}