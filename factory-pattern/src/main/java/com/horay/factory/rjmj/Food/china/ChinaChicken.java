package com.horay.factory.rjmj.Food.china;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.factory.rjmj.Food.Chicken;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 7:31
 */
public class ChinaChicken extends Chicken {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public ChinaChicken(Integer number) {
		this.name = "中国炸鸡";
		this.price = 8F;
		this.number = number;
	}

}
