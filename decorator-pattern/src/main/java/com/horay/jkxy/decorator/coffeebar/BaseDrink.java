package com.horay.jkxy.decorator.coffeebar;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 6:27
 */
public abstract class BaseDrink {

	protected String description;

	protected Float price = 0.0F;

	public String getDescription() {
		return description + "-" + price;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public abstract Float cost();

}
