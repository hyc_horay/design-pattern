package com.horay.jkxy.appliances;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:19
 */
public class AirConditioning extends Appliances {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private Integer temperature = 0;

	public AirConditioning(String name) {
		super(name);
	}

	public void up() {
		temperature++;
		logger.info("{}温度={}", name, temperature);
	}

	public void down() {
		temperature--;
		logger.info("{}温度={}", name, temperature);
	}

}
