package com.horay.jkxy.adapter;

import com.horay.jkxy.animal.Duck;
import com.horay.jkxy.animal.Turkey;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 0:14
 */
public class ObjectAdapter implements Duck {

	private Turkey turkey;

	public ObjectAdapter(Turkey turkey) {
		this.turkey = turkey;
	}

	@Override
	public void quack() {
		turkey.called();
	}

	@Override
	public void fly() {
		for (int i = 0; i < 6; i++) {
			turkey.fly();
		}
	}

}
