package com.horay.jkxy.appliances;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 10:50
 */
public class DVDPlayer {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public void on() {
		logger.info("开DVD播放机");
	}

	public void off() {
		logger.info("关DVD播放机");
	}

	public void play() {
		logger.info("播放DVD");
	}

	public void pause() {
		logger.info("暂停DVD");
	}

	public void setDvd() {
		logger.info("设置DVD");
	}

}
