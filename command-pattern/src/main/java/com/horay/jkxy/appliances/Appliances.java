package com.horay.jkxy.appliances;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/24 21:16
 */
public class Appliances {

	private Logger logger = LoggerFactory.getLogger(getClass());

	protected String name;

	public Appliances() {
	}

	public Appliances(String name) {
		this.name = name;
	}

	public void on() {
		logger.info("{} ON", name);
	}

	public void off() {
		logger.info("{} OFF", name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
