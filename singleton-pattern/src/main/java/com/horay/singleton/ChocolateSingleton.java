package com.horay.singleton;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/23 10:08
 */
public class ChocolateSingleton {

	private boolean empty;

	private boolean boiled;

	private static volatile ChocolateSingleton chocolateFactory = null;

	private ChocolateSingleton() {
		this.empty = true;
		this.boiled = false;
	}

	public static synchronized ChocolateSingleton getInstance() {
		if (chocolateFactory == null) {
			synchronized (ChocolateSingleton.class) {
				if (chocolateFactory == null) {
					chocolateFactory = new ChocolateSingleton();
				}
			}
		}
		return chocolateFactory;
	}

	public void fill() {
		if (empty) {
			// 添加原料巧克力动作
			empty = false;
			boiled = false;
		}
	}

	public void drain() {
		if ((!empty) && boiled) {
			// 排出巧克力动作
			empty = true;
		}
	}

	public void boil() {
		if ((!empty) && (!boiled)) {
			// 煮沸
			boiled = true;
		}
	}

}
