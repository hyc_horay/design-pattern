package com.horay.factory.jkxy.factory;

import com.horay.factory.PizzaTest;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/23 21:24
 */
public class OrderFactoryTest extends PizzaTest {

	public static void main(String[] args) throws InterruptedException {

		OrderPizza orderPizza = new OrderPizza();

		OrderFactory orderFactory = null;
		do {
			String pizzaType = getPizzaType();
			if (pizzaType.startsWith("London")) {
				pizzaType = pizzaType.replace("London", "");
				orderFactory = new LondonOrderFactory();
			} else if (pizzaType.startsWith("NewYork")) {
				pizzaType = pizzaType.replace("NewYork", "");
				orderFactory = new NewYorkOrderFactory();
			} else {
				continue;
			}
			orderPizza.setOrderFactory(orderFactory);
			orderPizza.getPizza(pizzaType);
		} while (true);

	}

}