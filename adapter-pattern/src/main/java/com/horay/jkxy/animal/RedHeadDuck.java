package com.horay.jkxy.animal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author heyingcheng
 * @category Class description
 * @email horay_hyc@qq.com
 * @date 2017/12/25 0:08
 */
public class RedHeadDuck implements Duck {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void quack() {
		logger.info("gua gua");
	}

	@Override
	public void fly() {
		logger.info("long distance");
	}

}
