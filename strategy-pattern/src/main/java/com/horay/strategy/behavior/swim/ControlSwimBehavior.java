package com.horay.strategy.behavior.swim;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.strategy.duck.Duck;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 19:03
 */
public class ControlSwimBehavior implements SwimBehavior {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void swim(Duck duck) {
		logger.info("{}操作控制器游", duck.getName());
	}

}
