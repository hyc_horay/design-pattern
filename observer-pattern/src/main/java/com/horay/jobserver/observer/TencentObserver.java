package com.horay.jobserver.observer;

import java.util.Observable;
import java.util.Observer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.horay.observer.model.WeatherData;

/**
 * @category Class description
 * @author heyingcheng
 * @email horay_hyc@qq.com
 * @date 2017/12/22 20:23
 */
public class TencentObserver implements Observer {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void update(Observable o, Object arg) {
		WeatherData weatherData = (WeatherData) arg;
		logger.info("{}：今天的温度{}℃", TencentObserver.class.getName(), weatherData.getTemperature());
		logger.info("{}：今天的湿度{}%", TencentObserver.class.getName(), weatherData.getHumidity());
		logger.info("{}：今天的气压{}%", TencentObserver.class.getName(), weatherData.getPressure());
	}
}
